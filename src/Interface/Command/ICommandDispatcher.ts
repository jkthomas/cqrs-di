export interface ICommandDispatcher<TCommand, TCommandHandler> {
  commandHandlersPrefix: string;
  commandHandlersSuffix: string;
  commandHandlers: TCommandHandler[];
  dispatch(command: TCommand): Promise<void>;
}
