import { QueryData } from './QueryData';

export interface IQueryDispatcher<
  TQuery,
  TQueryData extends QueryData,
  TQueryExecutor
> {
  queryExecutorsPrefix: string;
  queryExecutorsSuffix: string;
  queryExecutors: TQueryExecutor[];
  dispatch(query: TQuery): Promise<TQueryData>;
}
