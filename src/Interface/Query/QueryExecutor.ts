import { Query } from '../../Infrastructure/Query/Query';
import { QueryData } from './QueryData';

export interface QueryExecutor<
  TQuery extends Query,
  TQueryData extends QueryData
> {
  execute(query: TQuery): Promise<TQueryData>;
}
