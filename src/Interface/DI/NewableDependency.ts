export interface NewableDependency<T> {
  new (): T;
}
