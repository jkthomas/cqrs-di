export interface IDependencyProvider<T, TTypeName> {
  provideByTypeName(
    typeName: TTypeName,
    prefix: string,
    suffix: string,
  ): T | null;
}
