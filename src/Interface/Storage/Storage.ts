import { Entity } from '../../Domain/Entity';

export interface Storage<TStorage extends Entity> {
  find: (ids: string[]) => Promise<TStorage[] | null>;
  findOne: (id: string) => Promise<TStorage | null>;
  create: (entity: TStorage) => Promise<TStorage | null>;
  update: (entity: TStorage) => Promise<TStorage | null>;
  remove: (id: string) => Promise<string | null>;
}
