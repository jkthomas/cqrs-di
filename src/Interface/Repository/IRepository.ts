import { Entity } from '../../Domain/Entity';

export interface IRepository<TRepository extends Entity> {
  find: (ids: string[]) => Promise<TRepository[] | null>;
  findOne: (id: string) => Promise<TRepository | null>;
  create: (entity: TRepository) => Promise<TRepository | null>;
  update: (entity: TRepository) => Promise<TRepository | null>;
  remove: (id: string) => Promise<string | null>;
}
