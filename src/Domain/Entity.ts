export abstract class Entity {
  protected _id: string = '';

  constructor(id: string) {
    this._id = id;
  }

  public get id(): string {
    return this._id;
  }

  private setId(id: string): void {
    this._id = id;
  }
}
