export abstract class Query {
  public name: string;

  constructor(name: string) {
    this.name = name;
  }
}
