import { IQueryDispatcher } from '../../Interface/Query/IQueryDispatcher';
import { NewableDependency } from '../../Interface/DI/NewableDependency';
import { DependencyProvider } from '../DI/DependencyProvider';
import { QueryData } from '../../Interface/Query/QueryData';
import { Query } from './Query';
import { QueryExecutor } from '../../Interface/Query/QueryExecutor';
import { UnregisteredDependencyError } from '../Error/UnregisteredDependencyError';
import { UndefinedInputError } from '../Error/UndefinedInputError';

export class QueryDispatcher<
  TQuery extends Query,
  TQueryExecutor extends NewableDependency<QueryExecutor<TQuery, QueryData>>
> implements IQueryDispatcher<TQuery, QueryData, TQueryExecutor> {
  queryExecutors: TQueryExecutor[];
  queryExecutorsPrefix: string;
  queryExecutorsSuffix: string;

  constructor(
    queryExecutors: TQueryExecutor[],
    queryExecutorsPrefix: string = '',
    queryExecutorsSuffix: string = 'Executor',
  ) {
    this.queryExecutors = queryExecutors;
    this.queryExecutorsPrefix = queryExecutorsPrefix;
    this.queryExecutorsSuffix = queryExecutorsSuffix;
  }

  async dispatch(query: TQuery): Promise<QueryData> {
    if (!query) {
      throw new UndefinedInputError('Input query is not defined!');
    }
    // TODO: Switch to abstraction/interface (add DI module)
    const executor = new DependencyProvider(
      this.queryExecutors,
    ).provideByTypeName(
      query.name,
      this.queryExecutorsPrefix,
      this.queryExecutorsSuffix,
    );

    if (!executor) {
      throw new UnregisteredDependencyError(
        `Query executor of type ${typeof query} cannot be resolved`,
      );
    }
    const result = await executor.execute(query);
    return result;
  }
}
