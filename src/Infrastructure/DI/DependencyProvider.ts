import { IDependencyProvider } from '../../Interface/DI/IDependencyProvider';
import { NewableDependency } from '../../Interface/DI/NewableDependency';

export class DependencyProvider<T, TTypeName>
implements IDependencyProvider<T, TTypeName> {
  private dependencies: NewableDependency<T>[];

  constructor(dependencies: NewableDependency<T>[]) {
    this.dependencies = dependencies;
  }

  provideByTypeName(
    typeName: TTypeName,
    prefix: string = '',
    suffix: string = '',
  ): T | null {
    const dependency = this.dependencies.find(
      (dep: NewableDependency<T>) =>
        dep.name === `${prefix}${typeName}${suffix}`,
    );
    if (!dependency) {
      console.error(
        `No dependency ${prefix}${typeName}${suffix} registered in provider.`,
      );
      return null;
    }
    return new dependency();
  }
}
