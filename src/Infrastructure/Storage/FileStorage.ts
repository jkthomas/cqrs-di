import { promisify } from 'util';
import fs from 'fs';

import { Entity } from '../../Domain/Entity';
import { Storage } from '../../Interface/Storage/Storage';

const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const unlink = promisify(fs.unlink);

export class FileStorage<TStorage extends Entity> implements Storage<TStorage> {
  private readonly path: string;
  private readonly encoding: string;

  constructor(filepath: string) {
    this.path = filepath;
    this.encoding = 'utf-8';
  }

  public async find(ids: string[]): Promise<TStorage[] | null> {
    const paths = ids.map((id: string) => {
      return `${this.path}/${id}.json`;
    });

    const raw = paths.map(async (path: string) => {
      return await readFile(path, this.encoding);
    });

    let result: TStorage[] | null = null;

    Promise.all(raw)
      .then((data: string[]) => {
        result = data.map((item: string) => JSON.parse(item));
      })
      .catch((error: Error) => {
        console.error('Failed reading multiple files: ', error);
      });

    return result;
  }

  public async findOne(id: string): Promise<TStorage | null> {
    const dataPath = `${this.path}/${id}.json`;

    let result: TStorage | null = null;

    await readFile(dataPath, this.encoding)
      .then((data: string) => {
        result = JSON.parse(data);
      })
      .catch((error: Error) => {
        console.error('Failed reading file: ', error);
      });

    return result;
  }

  public async create(data: TStorage): Promise<TStorage | null> {
    const dataPath = `${this.path}/${data.id}.json`;

    let result: TStorage | null = null;

    await writeFile(dataPath, JSON.stringify(data), this.encoding)
      .then(() => {
        result = data as TStorage;
      })
      .catch((error: Error) => {
        console.error('Failed writing file: ', error);
      });

    return result;
  }

  public async update(data: TStorage): Promise<TStorage | null> {
    const dataPath = `${this.path}/${data.id}.json`;

    let result: TStorage | null = null;

    await readFile(dataPath, this.encoding).catch((error: Error) => {
      throw new Error(`Failed updating file: ${error}`);
    });

    await writeFile(dataPath, JSON.stringify(data), this.encoding)
      .then(() => {
        result = data as TStorage;
      })
      .catch((error: Error) => {
        console.error('Failed writing file: ', error);
      });

    return result;
  }

  public async remove(id: string): Promise<string | null> {
    const dataPath = `${this.path}/${id}.json`;

    let result: string | null = null;

    await unlink(dataPath)
      .then(() => {
        result = id;
      })
      .catch((error: Error) => {
        console.error('Failed removing file: ', error);
      });

    return result;
  }
}
