import { CommandHandler } from '../../Interface/Command/CommandHandler';
import { ICommandDispatcher } from '../../Interface/Command/ICommandDispatcher';
import { NewableDependency } from '../../Interface/DI/NewableDependency';
import { DependencyProvider } from '../DI/DependencyProvider';
import { UndefinedInputError } from '../Error/UndefinedInputError';
import { UnregisteredDependencyError } from '../Error/UnregisteredDependencyError';
import { Command } from './Command';

export class CommandDispatcher<
  TCommand extends Command,
  TCommandHandler extends NewableDependency<CommandHandler<TCommand>>
> implements ICommandDispatcher<TCommand, TCommandHandler> {
  commandHandlers: TCommandHandler[];
  commandHandlersPrefix: string;
  commandHandlersSuffix: string;

  constructor(
    commandHandlers: TCommandHandler[],
    commandHandlersPrefix: string = '',
    commandHandlersSuffix: string = 'Handler',
  ) {
    this.commandHandlers = commandHandlers;
    this.commandHandlersPrefix = commandHandlersPrefix;
    this.commandHandlersSuffix = commandHandlersSuffix;
  }

  async dispatch(command: TCommand): Promise<void> {
    if (!command) {
      throw new UndefinedInputError('Input command is not defined!');
    }
    // TODO: Switch to abstraction/interface (add DI module)
    const handler = new DependencyProvider(
      this.commandHandlers,
    ).provideByTypeName(command.name, '', 'Handler');

    if (!handler) {
      throw new UnregisteredDependencyError(
        `Command handler of type ${typeof command} cannot be resolved`,
      );
    }
    await handler.handle(command);
  }
}
