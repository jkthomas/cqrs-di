import { Entity } from '../../Domain/Entity';
import { Storage } from '../../Interface/Storage/Storage';
import { IRepository } from '../../Interface/Repository/IRepository';

export class Repository<TRepository extends Entity>
implements IRepository<TRepository> {
  private storage: Storage<TRepository>;

  constructor(storage: Storage<TRepository>) {
    this.storage = storage;
  }

  public async find(ids: string[]): Promise<TRepository[] | null> {
    return this.storage.find(ids);
  }

  public async findOne(id: string): Promise<TRepository | null> {
    return this.storage.findOne(id);
  }

  public async create(entity: TRepository): Promise<TRepository | null> {
    return this.storage.create(entity);
  }

  public async update(entity: TRepository): Promise<TRepository | null> {
    return this.storage.update(entity);
  }

  public async remove(id: string): Promise<string | null> {
    return this.storage.remove(id);
  }
}
